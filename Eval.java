//
// Eval.java: eval expressions
//

import lexer.*;
import node.*;
import parser.*;
import analysis.*;
import java.io.*;

// in this case, all we need to do is set what happens when "exit"
//    nodes (since depth-first traversal will guarantee subexpressions
//    are computed first)
public class Eval extends DepthFirstAdapter
{
  protected int value;
  public void setValue(int v) { value = v; }
  public void setValue(String v) { value = Integer.parseInt(v); }
  public int getValue() { return value; }
        
  public void caseAAddExp(AAddExp e)
  {
    e.getExp().apply(this);
    Eval right = new Eval();
    e.getTerm().apply(right);
    value += right.getValue();
  }
        
  public void caseASubtExp(ASubtExp e)
  {
    e.getExp().apply(this);
    Eval right = new Eval();
    e.getTerm().apply(right);
    value -= right.getValue();
  }
        
  public void caseATimesTerm(ATimesTerm e)
  {
    e.getTerm().apply(this);
    Eval right = new Eval();
    e.getFactor().apply(right);
    value *= right.getValue();
  }
        
  public void caseADivTerm(ADivTerm e)
  {
    e.getTerm().apply(this);
    Eval right = new Eval();
    e.getFactor().apply(right);
    value /= right.getValue();
  }
        
  // covers caseANumFactor
  public void caseTNumber(TNumber n)
  {
    setValue(n.getText());
  }

  public static void main(String[] args)
  {
    try
    {
      Reader reader;
      if ( args.length >= 1 )
        reader = new FileReader(args[0]);
      else
        reader = new InputStreamReader(System.in);
      // create lexer, parser, and abstract syntax tree
      Lexer lexer = new Lexer(new PushbackReader(new BufferedReader(reader),
                                                 1024));
      Parser parser = new Parser(lexer);
      Start ast = parser.parse();
      reader.close();

      // eval nodes in ast
      Eval c = new Eval();
      ast.apply(c);
      System.out.println("Eval: " + c.getValue());

    }
    catch (Exception e)
    {
      System.out.println(e);
      // in a production system, would call something like System.exit(1);
    }
  }
}
