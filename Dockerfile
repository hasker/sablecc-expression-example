#
# DockerFile for sablecc-expression-examplel
#   Use: 
#       docker build --progress=plain ./
#   in the folder containing this file.
#
# /usr/share/java/sablecc-3.7.jar
#

FROM lamtvb/sablecc3.7

RUN apt-get update; apt-get install -y make

COPY . /build

RUN cd /build; make test
