
all: setup Echo.class Eval.class

JARFILE=/sablecc.jar

setup:
	mkdir -p lexer

test: echo_runs.out eval_runs.out

lexer/Lexer.java: exp.grammar
	java -jar $(JARFILE) exp.grammar

Echo.class: Echo.java lexer/Lexer.java
	javac Echo.java

Eval.class: Eval.java lexer/Lexer.java
	javac Eval.java

echo_runs.out: Echo.class exp1 exp2 expbad
	java Echo exp1 > echo_runs.out
	java Echo exp2 >> echo_runs.out
	java Echo expbad >> echo_runs.out
	diff echo_runs.out echo_runs.expected
	@echo "*** Echo tests pass."

eval_runs.out: Eval.class exp1 exp2 expbad
	java Eval exp1 > eval_runs.out
	java Eval exp2 >> eval_runs.out
	java Eval expbad >> eval_runs.out
	diff eval_runs.out eval_runs.expected
	@echo "*** Eval tests pass."

clean:
	rm -rf analysis lexer node parser
	rm -f echo_runs.out eval_runs.out
	rm -f *.class
