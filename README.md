# sablecc-expression-example

Sample project using sablecc to print and evaluate expressions

Run:
```
        docker build .
```

A successful run will have two outputs:
```
        *** Echo tests pass.
        *** Eval tests pass.
```
The first is the result of running `Echo.java` on `exp1`, `exp2`, and then
`expbad`.  The output is captured and compared against
`echo_runs.expected`. The second run is the result of running `Eval.java` on
the same to get `eval_runs.expected`. Diff is then used to compare results.

Notes:
* This container is based on Java 8, but the code is not strongly dependent on
that version.
* The current build script uses Sable 3.7, but other versions would likely
work as well.
