//
// Echo.java: echo expressions
//

import lexer.*;
import node.*;
import parser.*;
import analysis.*;
import java.io.*;

// in this case, all we need to do is set what happens when "exit"
//    nodes (since depth-first traversal will guarantee subexpressions
//    are computed first)
public class Echo extends DepthFirstAdapter
{
  protected String result = "";
  public void setResult(String s) { result = s; }
  public String getResult() { return result; }
        
  public void caseAAddExp(AAddExp e)
  {
    e.getExp().apply(this);
    result += " + ";
    e.getTerm().apply(this);
  }
        
  public void caseASubtExp(ASubtExp e)
  {
    e.getExp().apply(this);
    result += " - ";
    e.getTerm().apply(this);
  }
        
  public void caseATimesTerm(ATimesTerm e)
  {
    e.getTerm().apply(this);
    result += " * ";
    e.getFactor().apply(this);
  }
        
  public void caseADivTerm(ADivTerm e)
  {
    e.getTerm().apply(this);
    result += " / ";
    e.getFactor().apply(this);
  }
        
  public void caseANestedFactor(ANestedFactor e)
  {
    result += "(";
    e.getExp().apply(this);
    result += ")";
  }

  // covers caseANumFactor
  public void caseTNumber(TNumber n)
  {
    result += n.getText();
  }

  public static void main(String[] args)
  {
    try
    {
      Reader reader;
      if ( args.length >= 1 )
        reader = new FileReader(args[0]);
      else
        reader = new InputStreamReader(System.in);
      // create lexer, parser, and abstract syntax tree
      Lexer lexer = new Lexer(new PushbackReader(new BufferedReader(reader),
                                                 1024));
      Parser parser = new Parser(lexer);
      Start ast = parser.parse();
      reader.close();

      // echo nodes in ast
      Echo c = new Echo();
      ast.apply(c);
      System.out.println("Echo: " + c.getResult());
    }
    catch (Exception e)
    {
      System.out.println(e);
      // in a production system, would call something like System.exit(1);
    }
  }
}
